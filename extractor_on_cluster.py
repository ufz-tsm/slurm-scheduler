#!/usr/bin/env python
from __future__ import annotations

import os
import sys

import requests
import json
import logging


script = """#!/bin/bash
cd /home/sontsm
source venv/bin/activate
python tsm-extractor/src/main.py {args}
"""

if "--help" in sys.argv or len(sys.argv) == 1:
    name = os.path.basename(__file__)
    print(
        f"Usage: {name} [ARGS]...\n"
        f"Run tsm-extractor on the cluster.\n"
        f"Example: {name} parse -t TARGET -d DEVICE -u USER -pw PWD\n"
        f"All ARGS are passed to the extractor.\n"
        f"The environment variable 'SLURM_JWT' must be set."
    )
    exit(2)

jwt = os.environ.get("SLURM_JWT")
if jwt is None:
    raise EnvironmentError("Environment Variable 'SLURM_JWT' must be set")

headers = {
    "X-SLURM-USER-NAME": "sontsm",
    "X-SLURM-USER-TOKEN": jwt,
}


def _ping():  # For testing only
    r = requests.get("http://localhost:6820/slurm/v0.0.38/ping", headers=headers)
    logging.info(json.dumps(r.json(), indent=2))
    r.raise_for_status()


def submit_job(args: str):
    """
    args must be all args that to pass to tsm-extractor/scr/main.py

    eg.
    "parse -p CsvParser -t TARGET -s SOURCE -d DEVICE -m 127.0.0.1:1883 -u mqtt -pw mqtt"
    """
    url = "http://localhost:6820/slurm/v0.0.38/job/submit"
    job = {
        "script": script.format(args=args),
        "job": {
            "environment": {
                "USER": "sontsm",
            },
            "standard_output": "/work/sontsm/job-%j.out",
            "standard_error": "/work/sontsm/job-%j.err",
        },
    }
    response = requests.post(url, headers=headers, json=job)
    logging.info(json.dumps(response.json(), indent=2))
    response.raise_for_status()
    logging.info("Successfully submitted a job")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    args = sys.argv[1:]

    # quote values again
    for i in range(len(args[1::2])):
        args[i * 2 + 1] = f'"{args[i*2+1]}"'

    logging.debug(args)
    submit_job(" ".join(args))
