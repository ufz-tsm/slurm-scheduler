#!/usr/bin/env python
from __future__ import annotations
import sys
import time
import jwt

USAGE = """Usage: mkJWT.py USER EXPIRATION_TIME KEY_FILE
Create a JSON Web Token (JWT) for given user.
Example: mkJWT.py bob 3600 jwt_hs256.key

Parameter:
   USER                 the user name
   EXPIRATION_TIME      time in seconds for the token to be valid
   KEY_FILE             path to file containing the secret HS256 key
"""


if len(sys.argv) != 4:
    print(USAGE)
    sys.exit(2)
else:
    user, seconds, keyfile = sys.argv[1:]

with open(keyfile, "rb") as f:
    key = f.read()

payload = {
    "exp": int(time.time()) + int(seconds),
    "iat": int(time.time()),
    "sun": user,
}

token = jwt.encode(payload, key, algorithm="HS256")
print(token)
