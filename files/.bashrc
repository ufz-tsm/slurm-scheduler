# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi
alias ls='ls --color=auto'
alias ll='ls --color=auto -lAhF'
alias ..='cd ..'
set -o vi
