# Slurm Scheduler Docker Image

This image provides a minimal EVE cluster mock with a REST-API to interact with
[Slurm](https://slurm.schedmd.com/) on the cluster. 

The image provide a `CentOS 7` system and has `Python 3.8.6` installed, just like the 
currently (Mai 2023) latest pre-installed version on the cluster.

## TL;DR

#### build
```shell
cp credentials/jwt_hs256.key.example credentials/jwt_hs256.key
docker compose build slurm-scheduler  
# this might take a while
```

#### start container
```shell
docker compose up -d
```

#### Rest API
```shell
USER_NAME='sontsm'
SLURM_JWT=$(./mkJWT.py sontsm 36000 credentials/jwt_hs256.key)
# test connection
curl localhost:6820/slurm/v0.0.38/ping -H "X-SLURM-USER-NAME:$USER_NAME" -H "X-SLURM-USER-TOKEN:$SLURM_JWT" 
# submit a job
curl -X POST localhost:6820/slurm/v0.0.38/job/submit -H "X-SLURM-USER-NAME:$USER_NAME" -H "X-SLURM-USER-TOKEN:$SLURM_JWT" -H "Content-Type: application/json" -d @submit_example.json 
```

Or with python
```shell
export SLURM_JWT 
extractor_on_cluster.py parse -t TARGET -s SOURCE ...
# see also
extractor_on_cluster.py --help
```

Inspect results on `/work/sontsm` within the cluster, with:
```shell
docker compose exec slurm-scheduler /bin/bash
```


## Usage


### Start/Stop the container

If we want to start the container for the first time we need to provide some 
credentials. For testing simply copy the `.example` files.

```shell
cp credentials/jwt_hs256.key.example credentials/jwt_hs256.key 
```

#### start container

Start the container and observe the logs until 
`Cluster is now available` appears.
```shell
docker compose up -d 
```
```shell
docker compose logs --follow
```
Now the cluster and the *Slurm Rest API* are available, and we can Hit `Ctrl-C` to 
exit the last command.

#### stop container

To later stop the container, we can use
```shell
docker compose down --remove-orphans --timeout 0
```

### Credentials for the REST-API

To send requests against the REST-API we first need some authentication, so we use
the prepared python script to get our token.
```shell
USER_NAME=sontsm
SLURM_JWT=$(./mkJWT.py $USER_NAME 36000 credentials/jwt_hs256.key)
```
This will set the environment variables `SLURM_JWT` and `USER_NAME`, which we 
use to authenticate us in the following examples.

### Testing the Setup / Ping the REST_API
> **See also:**  https://slurm.schedmd.com/rest_api.html#slurmV0038Ping

To see if the container is up and the credentials are set correctly run the following
with `curl`:
```shell
curl localhost:6820/slurm/v0.0.38/ping -H "X-SLURM-USER-NAME:$USER_NAME" -H "X-SLURM-USER-TOKEN:$SLURM_JWT" 
```

On success, we should see a json response containing the following snippet:
```json
{
  ...
   "pings": [
     {
       "hostname": "slurmctl",
       "ping": "UP",
       "status": 0,
       "mode": "primary"
     }
   ],
  ...
}
```

### Submitting a job / Hello world
> **See also:**  https://slurm.schedmd.com/rest_api.html#slurmV0038SubmitJob

To submit a job we have to make a POST request and send some `json` as payload. 
The (shell) submit script, one might know from slurm, must also be sent entirely as a 
string. In other words: the full file content e.g. starting with `#!/bin/bash` must be 
wrapped as a string, with linebreaks replaced as `\n`. For example, the 
example-hallo-world line look like this:
```json
"script": "#!/bin/bash\necho Hello world",
```

To actually submit a simple job run the following:

```shell
curl -X POST localhost:6820/slurm/v0.0.38/job/submit -H "X-SLURM-USER-NAME:$USER_NAME" -H "X-SLURM-USER-TOKEN:$SLURM_JWT" -H "Content-Type: application/json" -d @submit_example.json 
```

The results will be written to files in the `/work/$USER_NAME` directory and called
`job-N.err` for output on `stderr` and `job-N.out` for output on `stdout`. 
To actually see the result, we have to peek inside the container with
```shell
docker compose exec slurm-scheduler cat /work/$USER_NAME/job-1.out
```

> **Hint:** 
> Its might be easier to use `docker compose exec slurm-scheduler /bin/bash` in an 
> extra terminal window, to connect with a shell inside the container. 
> With that we can easily navigate to `/work/sontsm` and investigate all job files. 



## Development Hints

This project is a modified fork of https://github.com/giovtorres/docker-centos7-slurm .

🙏 Thanks @giovtorres 🙏 for this helpful work.

As stated in the README there the image's hostname must be `slurmctl`, 
otherwise it won't work.

The main modifications we are made:
- other versions:
    - slurm: `22-05-8-1`
    - openssl: `1.1.1t`
    - python3: `3.8.6`
- only a single python3 version
- no pyenv
- mount JWT key instead of generating it on build


